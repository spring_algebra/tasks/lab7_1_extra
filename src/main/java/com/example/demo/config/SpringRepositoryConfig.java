/*
 * Algebra labs.
 */
 
package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.example.demo.persistence.InMemoryItemRepository;
import com.example.demo.persistence.ItemRepository;
import com.example.demo.persistence.JpaItemRepository;

// Declare as a configuration class
@Configuration
public class SpringRepositoryConfig {
	
	// Declare the item repository bean
	@Bean @Profile("inMemory")
	public ItemRepository itemRepository() {
		return new InMemoryItemRepository();
	}
	
	@Bean @Profile("jpa")
	public ItemRepository jpaItemRepository() {
		return new JpaItemRepository();
	}
	
}