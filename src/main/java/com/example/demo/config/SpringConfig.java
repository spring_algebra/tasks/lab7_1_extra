/*
 * Algebra labs.
 */
 
package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

// Declare as a Spring configuration class
// and import other configuration classes and XML config files
@Configuration
@EnableAspectJAutoProxy
@Import({SpringRepositoryConfig.class, SpringServicesConfig.class})
@ImportResource("/WEB-INF/configuration/applicationContext.xml")
public class SpringConfig {}