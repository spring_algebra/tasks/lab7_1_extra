/*
 * Algebra labs.
 */

package com.example.demo.service;

import java.util.Collection;

import com.example.demo.domain.MusicItem;

public interface Catalog {
   public void persist(MusicItem item);
   public void persistBatch(Collection<MusicItem> items);
   public void remove(MusicItem item);
   public MusicItem findById(Long id);
   public Collection<MusicItem> findByKeyword(String keyword);
   public int size();

}
